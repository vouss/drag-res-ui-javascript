$.fn.draggable = function() {
    var isMouseDown = false;
        
    function stopBubble(e) {
        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) {
            e.stopPropagation();
        }
    }
    
    var clickPosition = {
        x:null,
        y:null
    };

    var clickDraggablePosition = {
        x:null,
        y:null
    };
    
    var parent = this.parent();
    var element = this;
    

    this.mousedown(function(event) {
        isMouseDown = true;
        clickPosition.x = event.pageX;
        clickPosition.y = event.pageY;
        clickDraggablePosition.x = event.pageX - element.offset().left + parent.offset().left ;
        clickDraggablePosition.y = event.pageY - element.offset().top + parent.offset().top;
        
        element.addClass('active');
        event.stopPropagation();
    });

    $('body').mouseup(function() {
        clickPosition.x = clickPosition.y = null;
        element.removeClass('active');
    });

    $('body').mousemove(function( event ) {
        var movement = {
            x: event.pageX - clickDraggablePosition.x,
            y: event.pageY - clickDraggablePosition.y
        };

        if(!element.hasClass('active')) {
            return false;
        }
        if(movement.x < 0) {
            movement.x = 0;
        }
        if(movement.y < 0) {
            movement.y = 0;
        }
        if(movement.x > parent.width() - element.width() ) {
            movement.x = parent.width() - element.width();
        }
        if(movement.y > parent.height() - element.height()) {
            movement.y = parent.height() - element.height();
        }

        element
            .css('top', movement.y)
            .css('left', movement.x);
    });
};