$.fn.resizable = function () {
    var parent = this.parent();
    var element = this;
    var p = this;

    var body = $('body');
    var isMouseDown = false;
    var selectedCorner = null;

    p.addClass('resizable');
    p.append('<div class="resizer mid"></div>');
    p.append('<div class="resizer bottom"></div>');
    p.append('<div class="resizer right"></div>');

    var midRes = $('.mid');
    var rightRes = $('.right');
    var bottomRes = $('.bottom');

    var max = {
        h: null,
        w: null,
        big: false
    };
    window.max = max;


    var clickPosition = {
        x: null,
        y: null
    };

    var sizeBox = {
        widthBox: null,
        heightBox: null
    };
    window.sizeBox = sizeBox;

    var startPosition = {
        width: null,
        height: null
    };

    sizeBox.widthBox = this.css('width');
    sizeBox.heightBox = this.css('height');

    function checkChildren() { 
      var maxX = 0,
          maxY = 0;
      var el;
      var actWidth = 0,
          actHeight = 0;
        $('.parent *').not('.resizer').each(function(){
              
          el = $(this);
          window.el = el;
          actWidth = el.offset().top + el.height();
          console.log('Aktualna szerokosc ele: ' + actWidth);
          actHeight = el.offset().left + el.width();
          console.log('Aktualna wysokosc ele: ' + actHeight);
          if( actWidth > maxX ) {
            maxX = actWidth;
          }
          if( actHeight > maxY ) {
            maxY = actHeight;
          }
        });
        max.h = maxX - 2;
        max.w = maxY - 2;
        maxX = 0;
        maxY = 0;
    }
    rightRes.mousedown(function (event) {
        checkChildren();
        clickPosition.x = event.clientX;
        startPosition.width = parseInt(sizeBox.widthBox, 10);
        selectedCorner = 2; // 1 = mid, 2 = right, 3 = bottom
        isMouseDown = true;
        checkChildren();
    });
    bottomRes.mousedown(function (event) {
        checkChildren();
        clickPosition.y = event.clientY;
        startPosition.height = parseInt(sizeBox.heightBox, 10);
        selectedCorner = 3; // 1 = mid, 2 = right, 3 = bottom
        isMouseDown = true;
    });

    midRes.mousedown(function (event) {
        checkChildren();
        clickPosition.x = event.clientX;
        clickPosition.y = event.clientY;
        startPosition.width = parseInt(sizeBox.widthBox, 10);
        startPosition.height = parseInt(sizeBox.heightBox, 10);
        selectedCorner = 1; // 1 = mid, 2 = right, 3 = bottom
        isMouseDown = true;
    });
    body.mouseup(function (event) {
        isMouseDown = false;
    });

    body.mousemove(function (event) {
        if (!isMouseDown) {
            return;
        } else if (selectedCorner == 2) {
            sizeBox.widthBox = (startPosition.width + event.clientX - clickPosition.x);
        } else if (selectedCorner == 3) {
            sizeBox.heightBox = (startPosition.height + event.clientY - clickPosition.y);
        } else if (selectedCorner == 1) {
            sizeBox.widthBox = (startPosition.width + event.clientX - clickPosition.x);
            sizeBox.heightBox = (startPosition.height + event.clientY - clickPosition.y);
        }

        if(max.h >= sizeBox.heightBox || max.w >= sizeBox.widthBox) {
            return;
        }

        p.width(sizeBox.widthBox);
        p.height(sizeBox.heightBox);

    });
};